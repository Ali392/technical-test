const modalBarng = (data) => {
   return `
   <div class="modal fade" id="modal-barang" role="dialog" tabindex="-1">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header text-bg-primary border-0">
               <h5 class="modal-title font-weight-normal">${data.title}</h5>
               <button class="btn-close text-white" data-bs-dismiss="modal" type="button" aria-label="Close"></button>
            </div>
            <div class="modal-body">
               <div class="mb-2">
                  <label for="nama" class="form-label">Nama Barang</label>
                  <input type="text" class="form-control form-barang text-capitalize" id="name" name="name" placeholder="" value="${data.barang ? data.barang.name : ''}">
                  <div class="invalid-feedback"></div>
               </div>
               <div class="mb-2">
                  <label for="stock" class="form-label">Stok Barang</label>
                  <input type="text" class="form-control form-barang text-capitalize form-number" id="stock" name="stock" placeholder="" value="${data.barang ? data.barang.stock : ''}">
                  <div class="invalid-feedback"></div>
               </div>
               <label for="price" class="form-label">Harga Barang</label>
               <div class="input-group mb-2">
                  <span class="input-group-text">Rp</span>
                  <input type="text" class="form-control form-barang text-capitalize form-number" id="price" name="price" placeholder="" value="${data.barang ? data.barang.price : ''}">
                  <div class="invalid-feedback"></div>
               </div>

               <div class="mb-3">
                  <label for="formFile" class="form-label">Attachment</label>
                  <input class="form-control form-file" type="file" name="file" id="file" accept="application/pdf">
                  <div class="invalid-feedback"></div>
               </div>
            </div>
            <div class="modal-footer border-0">
               <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Cancel</button>
               <button class="btn btn-primary" type="submit" id="btn-submit">Submit</button>
            </div>
         </div>
      </div>
   </div>
   `
}

const modalShowBarang = (data) => {
    return `
    <div class="modal fade" id="modal-show-barang" role="dialog" tabindex="-1">
       <div class="modal-dialog" role="document">
          <div class="modal-content">
             <div class="modal-header text-bg-primary border-0">
                <h5 class="modal-title font-weight-normal">${data.title}</h5>
                <button class="btn-close text-white" data-bs-dismiss="modal" type="button" aria-label="Close"></button>
             </div>
             <div class="modal-body">
                <div class="mb-2">
                   <label for="nama" class="form-label">Nama Barang</label>
                   <input type="text" class="form-control form-barang text-capitalize" id="name" name="name" placeholder="" value="${data.barang ? data.barang.name : ''}" readonly>
                   <div class="invalid-feedback"></div>
                </div>
                <div class="mb-2">
                   <label for="stock" class="form-label">Stok Barang</label>
                   <input type="text" class="form-control form-barang text-capitalize form-number" id="stock" name="stock" placeholder="" value="${data.barang ? data.barang.stock : ''}" readonly>
                   <div class="invalid-feedback"></div>
                </div>
                <label for="price" class="form-label">Harga Barang</label>
                <div class="input-group mb-2">
                   <span class="input-group-text">Rp</span>
                   <input type="text" class="form-control form-barang text-capitalize form-number" id="price" name="price" placeholder="" value="${data.barang ? data.barang.price : ''}" readonly>
                   <div class="invalid-feedback"></div>
                </div>
             </div>
             <div class="modal-footer border-0">
               <button class="btn btn-danger" data-bs-dismiss="modal" type="button">Close</button>
            </div>
          </div>
       </div>
    </div>
    `
 }

const modalDeleteBarang = () => {
    return `
    <div class="modal fade" id="delete-modal" tabindex="-1">
    <div class="modal-dialog w-25">
       <div class="modal-content overflow-hidden">
          <div class="modal-body text-center">
             <h5>Yakin data akan dihapus?</h6>
             <p class="fs-6 mb-0">Anda tidak dapat mengembalikan data yang telah dihapus</p>
          </div>
          <form class="row justify-content-center align-items-center p-1 border-top" action="" method="" id="form-delete-barang">
             <div class="col-6 border-end">
                <button type="submit" class="btn btn-sm text-danger btn-transparent w-100 fs-6">Hapus</button>
             </div>
             <div class="col-6">
                <button type="button" class="btn btn-sm text-secondary bg-transparent w-100 fs-6" data-bs-dismiss="modal">Batal</button>
             </div>
          </form>
       </div>
    </div>
 </div>
   `
}

export {modalBarng,modalShowBarang, modalDeleteBarang}