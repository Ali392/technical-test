import { alert, reloadTable, requestData, showInvalidMessage } from "../global/global.js"
import { modalBarng, modalDeleteBarang, modalShowBarang } from "./menu-barang.js"

let tableBarang, isInsert, barangId, content
content = document.querySelector('.content')

tableBarang = $('#datatable-barang').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/barang",
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
        columns: [
            {data: 'DT_RowIndex', name: 'id', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'stock', name: 'stock'},
            {data: 'price', name: 'price'},
            {data: 'file', name: 'file'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        language: {
            lengthMenu: "_MENU_ Data per halaman",
            //zeroRecords: "Data tidak ditemukan",
            info: "Menampilkan halaman _PAGE_ dari _PAGES_",
            //infoEmpty: "Data kosong",
            infoFiltered: "(filter dari _MAX_ total data)",
            search: "Cari :",
    
            oPaginate: {
                sNext: '<i class="bi bi-arrow-right-short"></i>',
                sPrevious: '<i class="bi bi-arrow-left-short"></i>',
                sFirst: 'Pertama',
                sLast: 'Terakhir'
            },
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-12'i><'col-12'<'d-flex justify-content-center'p>>>",
        drawCallback: () => {
            //show data
            const btnShow = document.querySelectorAll('.show-data')
            btnShow.forEach(btn => btn.addEventListener('click', async function () {
                this.disabled = true
                barangId = this.dataset.id

                const data = {
                    title: 'Lihat Data Barang',
                }

                try {
                    const result = await requestData(`barang/${barangId}/edit`, 'GET')
                    data.barang = result.barang
    
                    content.insertAdjacentHTML('afterend', modalShowBarang(data))
                    const modalShow = document.getElementById('modal-show-barang')
                    const modal = new bootstrap.Modal(modalShow)
                    modal.show()

                    modalShow.addEventListener('hidden.bs.modal', () => {
                        modalShow.remove()
                        this.disabled = false
                    })
                } catch (error) {
                    alert('error', 'Gagal', error.message)
                }
            }))

            //edit data
            const btnEdit = document.querySelectorAll('.edit-data')
            btnEdit.forEach(btn => btn.addEventListener('click', async function (e) {
                this.disabled = true
                isInsert = false
                barangId = this.dataset.id

                const data = {
                    title: 'Edit Barang',
                }

                try {
                    const result = await requestData(`barang/${barangId}/edit`, 'GET')
                    data.barang = result.barang
    
                    content.insertAdjacentHTML('afterend', modalBarng(data))
                    const modalEdit = document.getElementById('modal-barang')
                    const modal = new bootstrap.Modal(modalEdit)
                    modal.show()
    
                    // batasi input angka
                    const inputNumber = document.querySelectorAll('.form-number')
                    inputNumber.forEach(input => input.addEventListener('input', function () {
                        this.value = this.value.replace(/[^0-9]/g, "")
                    }))
    
                    submit(isInsert, modal)
    
                    modalEdit.addEventListener('hidden.bs.modal', () => {
                        modalEdit.remove()
                        this.disabled = false
                    })
                } catch (error) {
                    alert('error', 'Gagal', error.message)
                }
            }))

            // Delete Barang
            const btnDelete = document.querySelectorAll('.delete-data')
            btnDelete.forEach(btn => btn.addEventListener('click', async function () {
                this.disabled = true
                barangId = this.dataset.id

                content.insertAdjacentHTML('afterend', modalDeleteBarang())
                const modalDelete = document.getElementById('delete-modal')
                const modal = new bootstrap.Modal(modalDelete)
                modal.show()
                const formDelete = document.querySelector('#form-delete-barang')
                formDelete.addEventListener('submit', async (e) => {
                    e.preventDefault()
                    modal.hide()
                    try {
                        const result = await requestData(`/barang/delete/${barangId}`, 'DELETE')
                        alert('success', 'Berhasil', result.message)
                    } catch (error) {
                        alert('error', 'Gagal', error.message) 
                    }

                    reloadTable(tableBarang)
                })
                modalDelete.addEventListener('hidden.bs.modal', () => {
                    modalDelete.remove()
                    this.disabled = false
                })
            }))
        }
    })

//tambah barang
const btnAdd = document.querySelector('#add-barang')
btnAdd.addEventListener('click', async function (e) {
    btnAdd.disabled = true
    isInsert = true

    const data = {
        title: 'Tambah barang'
    }

    try {
        content.insertAdjacentHTML('afterend', modalBarng(data))
        const modalAdd = document.getElementById('modal-barang')
        const modal = new bootstrap.Modal(modalAdd)
        modal.show()

        // batasi input angka
        const inputAngka = document.querySelectorAll('.form-number')
        inputAngka.forEach(input => input.addEventListener('input', function () {
            this.value = this.value.replace(/[^0-9]/g, "")
        }))

        submit(isInsert, modal)
        modalAdd.addEventListener('hidden.bs.modal', () => {
           modalAdd.remove()
           btnAdd.disabled = false
        })
    } catch (error) {
        alert('error', 'Gagal', error.message)
    }
  
})

const submit = (isInsert, modal) => {
    const btnSubmit = document.querySelector('#btn-submit')
    btnSubmit.addEventListener('click', async (e) => {
        e.preventDefault()

        const formData = new FormData()
        const formBarang = document.querySelectorAll('.form-barang')
        const formFile = document.querySelector('.form-file')

        formBarang.forEach(form => formData.append(form.getAttribute('name'), form.value))
        formData.append(formFile.getAttribute('name'), formFile.files[0])

        let url
        
        if (isInsert) {
            url = '/barang/store'
        } else {
            url = `/barang/update/${barangId}`
            formData.append('_method', 'PATCH')
        }

        try {
            const result = await requestData(url, 'POST', formData)
            alert('success', 'Berhasil', result.message)
            modal.hide()
            reloadTable(tableBarang)
        } catch (error) {
            // error validation
            if (error.status == 422) {
                showInvalidMessage('form-barang', error.validation)
                showInvalidMessage('form-file', error.validation)
                return false
            }
 
            alert('error', 'Gagal', error.message)
        }
    })

}