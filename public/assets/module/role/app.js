import { alert, reloadTable, requestData, showInvalidMessage } from "../global/global.js"
import { modalDeleteRole, modalRole } from "./menu-role.js"

let tableRole, isInsert, roleId, content
content = document.querySelector('.content')

tableRole = $('#datatable-role').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/role",
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
        columns: [
            {data: 'DT_RowIndex', name: 'id', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        language: {
            lengthMenu: "_MENU_ Data per halaman",
            //zeroRecords: "Data tidak ditemukan",
            info: "Menampilkan halaman _PAGE_ dari _PAGES_",
            //infoEmpty: "Data kosong",
            infoFiltered: "(filter dari _MAX_ total data)",
            search: "Cari :",
    
            oPaginate: {
                sNext: '<i class="bi bi-arrow-right-short"></i>',
                sPrevious: '<i class="bi bi-arrow-left-short"></i>',
                sFirst: 'Pertama',
                sLast: 'Terakhir'
            },
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-12'i><'col-12'<'d-flex justify-content-center'p>>>",
        drawCallback: () => {

            //edit data
            const btnEdit = document.querySelectorAll('.edit-data')
            btnEdit.forEach(btn => btn.addEventListener('click', async function (e) {
                this.disabled = true
                isInsert = false
                roleId = this.dataset.id

                const data = {
                    title: 'Edit Role',
                }

                try {
                    const result = await requestData(`role/${roleId}/edit`, 'GET')
                    data.role = result.role
    
                    content.insertAdjacentHTML('afterend', modalRole(data))
                    const modalEdit = document.getElementById('modal-role')
                    const modal = new bootstrap.Modal(modalEdit)
                    modal.show()
    
                    submit(isInsert, modal)
    
                    modalEdit.addEventListener('hidden.bs.modal', () => {
                        modalEdit.remove()
                        this.disabled = false
                    })
                } catch (error) {
                    alert('error', 'Gagal', error.message)
                }
            }))

            // Delete data
            const btnDelete = document.querySelectorAll('.delete-data')
            btnDelete.forEach(btn => btn.addEventListener('click', async function () {
                this.disabled = true
                roleId = this.dataset.id

                content.insertAdjacentHTML('afterend', modalDeleteRole())
                const modalDelete = document.getElementById('delete-modal')
                const modal = new bootstrap.Modal(modalDelete)
                modal.show()
                const formDelete = document.querySelector('#form-delete-role')
                formDelete.addEventListener('submit', async (e) => {
                    e.preventDefault()
                    modal.hide()
                    try {
                        const result = await requestData(`/role/delete/${roleId}`, 'DELETE')
                        alert('success', 'Berhasil', result.message)
                    } catch (error) {
                        alert('error', 'Gagal', error.message) 
                    }

                    reloadTable(tableRole)
                })
                modalDelete.addEventListener('hidden.bs.modal', () => {
                    modalDelete.remove()
                    this.disabled = false
                })
            }))
        }
    })

//tambah role
const btnAdd = document.querySelector('#add-role')
btnAdd.addEventListener('click', async function (e) {
    btnAdd.disabled = true
    isInsert = true

    const data = {
        title: 'Tambah role user'
    }

    try {
        content.insertAdjacentHTML('afterend', modalRole(data))
        const modalAdd = document.getElementById('modal-role')
        const modal = new bootstrap.Modal(modalAdd)
        modal.show()

        submit(isInsert, modal)
        modalAdd.addEventListener('hidden.bs.modal', () => {
           modalAdd.remove()
           btnAdd.disabled = false
        })
    } catch (error) {
        alert('error', 'Gagal', error.message)
    }
  
})

const submit = (isInsert, modal) => {
    const btnSubmit = document.querySelector('#btn-submit')
    btnSubmit.addEventListener('click', async (e) => {
        e.preventDefault()

        const formData = new FormData()
        const formRole = document.querySelector('.form-role')

        formData.append(formRole.getAttribute('name'), formRole.value)

        let url
        
        if (isInsert) {
            url = '/role/store'
        } else {
            url = `/role/update/${roleId}`
            formData.append('_method', 'PATCH')
        }

        try {
            const result = await requestData(url, 'POST', formData)
            alert('success', 'Berhasil', result.message)
            modal.hide()
            reloadTable(tableRole)
        } catch (error) {
            // error validation
            if (error.status == 422) {
                showInvalidMessage('form-role', error.validation)
                return false
            }
 
            alert('error', 'Gagal', error.message)
        }
    })

}