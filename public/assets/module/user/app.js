import { alert, reloadTable, requestData, showInvalidMessage } from "../global/global.js"
import { modalDeleteUser, modalUser } from "./menu-user.js"

let tableUser, isInsert, userId, content
content = document.querySelector('.content')

tableUser = $('#datatable-user').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/user",
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
        columns: [
            {data: 'DT_RowIndex', name: 'id', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'role_id', name: 'role_id'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        language: {
            lengthMenu: "_MENU_ Data per halaman",
            //zeroRecords: "Data tidak ditemukan",
            info: "Menampilkan halaman _PAGE_ dari _PAGES_",
            //infoEmpty: "Data kosong",
            infoFiltered: "(filter dari _MAX_ total data)",
            search: "Cari :",
    
            oPaginate: {
                sNext: '<i class="bi bi-arrow-right-short"></i>',
                sPrevious: '<i class="bi bi-arrow-left-short"></i>',
                sFirst: 'Pertama',
                sLast: 'Terakhir'
            },
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-12'i><'col-12'<'d-flex justify-content-center'p>>>",
        drawCallback: () => {

            //edit data
            const btnEdit = document.querySelectorAll('.edit-data')
            btnEdit.forEach(btn => btn.addEventListener('click', async function (e) {
                this.disabled = true
                isInsert = false
                userId = this.dataset.id

                const data = {
                    title: 'Edit User',
                }

                try {
                    const result = await requestData(`user/${userId}/edit`, 'GET')
                    data.user = result.user
                    data.roles = result.roles
    
                    content.insertAdjacentHTML('afterend', modalUser(data))
                    const modalEdit = document.getElementById('modal-user')
                    const modal = new bootstrap.Modal(modalEdit)
                    modal.show()
    
                    submit(isInsert, modal)
    
                    modalEdit.addEventListener('hidden.bs.modal', () => {
                        modalEdit.remove()
                        this.disabled = false
                    })
                } catch (error) {
                    alert('error', 'Gagal', error.message)
                }
            }))

            // Delete data
            const btnDelete = document.querySelectorAll('.delete-data')
            btnDelete.forEach(btn => btn.addEventListener('click', async function () {
                this.disabled = true
                userId = this.dataset.id

                content.insertAdjacentHTML('afterend', modalDeleteUser())
                const modalDelete = document.getElementById('delete-modal')
                const modal = new bootstrap.Modal(modalDelete)
                modal.show()
                const formDelete = document.querySelector('#form-delete-user')
                formDelete.addEventListener('submit', async (e) => {
                    e.preventDefault()
                    modal.hide()
                    try {
                        const result = await requestData(`/user/delete/${userId}`, 'DELETE')
                        alert('success', 'Berhasil', result.message)
                    } catch (error) {
                        alert('error', 'Gagal', error.message) 
                    }

                    reloadTable(tableUser)
                })
                modalDelete.addEventListener('hidden.bs.modal', () => {
                    modalDelete.remove()
                    this.disabled = false
                })
            }))
        }
    })

//tambah user
const btnAdd = document.querySelector('#add-user')
btnAdd.addEventListener('click', async function (e) {
    btnAdd.disabled = true
    isInsert = true

    const data = {
        title: 'Tambah akun user'
    }

    try {
        // get data roles
        const result = await requestData('/role/getAll', 'GET')
        data.roles = result.roles

        content.insertAdjacentHTML('afterend', modalUser(data))
        const modalAdd = document.getElementById('modal-user')
        const modal = new bootstrap.Modal(modalAdd)
        modal.show()

        submit(isInsert, modal)
        modalAdd.addEventListener('hidden.bs.modal', () => {
           modalAdd.remove()
           btnAdd.disabled = false
        })
    } catch (error) {
        alert('error', 'Gagal', error.message)
    }
  
})

const submit = (isInsert, modal) => {
    const btnSubmit = document.querySelector('#btn-submit')
    btnSubmit.addEventListener('click', async (e) => {
        e.preventDefault()

        const formData = new FormData()
        const formUser = document.querySelectorAll('.form-user')

        formUser.forEach(form => formData.append(form.getAttribute('name'), form.value))

        let url
        
        if (isInsert) {
            url = '/user/store'
        } else {
            url = `/user/update/${userId}`
            formData.append('_method', 'PATCH')
        }

        try {
            const result = await requestData(url, 'POST', formData)
            alert('success', 'Berhasil', result.message)
            modal.hide()
            reloadTable(tableUser)
        } catch (error) {
            // error validation
            if (error.status == 422) {
                showInvalidMessage('form-user', error.validation)
                return false
            }
 
            alert('error', 'Gagal', error.message)
        }
    })

}