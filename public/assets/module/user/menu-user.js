const modalUser = (data) => {
   let content = `
   <div class="modal fade" id="modal-user" role="dialog" tabindex="-1">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header text-bg-primary border-0">
               <h5 class="modal-title font-weight-normal">${data.title}</h5>
               <button class="btn-close text-white" data-bs-dismiss="modal" type="button" aria-label="Close"></button>
            </div>
            <div class="modal-body">
               <div class="mb-2">
                  <label for="nama" class="form-label">Nama</label>
                  <input type="text" class="form-control form-user" id="name" name="name" placeholder="" value="${data.user ? data.user.name : ''}">
                  <div class="invalid-feedback"></div>
               </div>

               <div class="mb-2">
                  <label for="nama" class="form-label">Email</label>
                  <input type="email" class="form-control form-user" id="email" name="email" placeholder="" value="${data.user ? data.user.email : ''}">
                  <div class="invalid-feedback"></div>
               </div>

               <div class="mb-2">
									<label for="role" class="form-label">Role</label>
                  <select class="form-select form-user" name="role_id" id="select_role">
                    <option value="">Pilih Role</option>`

                    // append data
                    data.roles.forEach(role => {
                            content += `<option value="${role.id}" ${data.user ? (data.user.role_id == role.id ? 'selected' : '') : ''}>${role.name}</option>`
                    })

                    content += `
            	  </select>
                  <div class="invalid-feedback"></div>
               </div>

               <div class="mb-2">
                    <label class="form-label">Password</label>
                    <input type="password" class="form-control form-user" id="password" name="password" placeholder="">
                    <div class="invalid-feedback"></div>
                </div>

                <div class="mb-2">
                    <label class="form-label">Password Confirmation</label>
                    <input type="password" class="form-control form-user" id="password-confirm" name="password_confirmation" placeholder="">
                    <div class="invalid-feedback"></div>
                </div>
            </div>
            <div class="modal-footer border-0">
               <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Cancel</button>
               <button class="btn btn-primary" type="submit" id="btn-submit">Submit</button>
            </div>
         </div>
      </div>
   </div>
   `

   return content
}

const modalDeleteUser = () => {
    return `
    <div class="modal fade" id="delete-modal" tabindex="-1">
    <div class="modal-dialog w-25">
       <div class="modal-content overflow-hidden">
          <div class="modal-body text-center">
             <h5>Yakin data akan dihapus?</h6>
             <p class="fs-6 mb-0">Anda tidak dapat mengembalikan data yang telah dihapus</p>
          </div>
          <form class="row justify-content-center align-items-center p-1 border-top" action="" method="" id="form-delete-user">
             <div class="col-6 border-end">
                <button type="submit" class="btn btn-sm text-danger btn-transparent w-100 fs-6">Hapus</button>
             </div>
             <div class="col-6">
                <button type="button" class="btn btn-sm text-secondary bg-transparent w-100 fs-6" data-bs-dismiss="modal">Batal</button>
             </div>
          </form>
       </div>
    </div>
 </div>
   `
}

export { modalUser, modalDeleteUser }