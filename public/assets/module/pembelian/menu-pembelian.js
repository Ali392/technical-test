const modalPembelian = (data) => {
   let content = `
   <div class="modal fade" id="modal-pembelian" role="dialog" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header text-bg-primary border-0">
               <h5 class="modal-title font-weight-normal">${data.title}</h5>
               <button class="btn-close text-white" data-bs-dismiss="modal" type="button" aria-label="Close"></button>
            </div>
            <div class="modal-body">
							<div class="row">

								<div class="col-12 col-md-6 mb-2">
                  <label for="stock" class="form-label">Tanggal</label>
                  <input type="date" class="form-control form-pembelian" id="date" name="date" placeholder="" value="${data.pembelian ? data.pembelian.date : ''}">
                  <div class="invalid-feedback"></div>
               	</div>
						
                <div class="col-12 col-md-6 mb-2">
									<label for="nama" class="form-label">Nama Barang</label>
                  <select class="form-select form-pembelian" name="barang_id" id="select_barang">
                    <option value="">Pilih Barang</option>`

										// append data
										data.barangs.forEach(barang => {
												content += `<option value="${barang.id}" ${data.pembelian ? (data.pembelian.barang_id == barang.id ? 'selected' : '') : ''}>${barang.name}</option>`
										})

										content += `
            			</select>
                  <div class="invalid-feedback"></div>
               </div>

               <div class="col-12 col-md-6 mb-2">
                  <label for="stock" class="form-label">QTY Barang</label>
                  <input type="text" class="form-control form-pembelian text-capitalize form-number" id="qty" name="qty" placeholder="" value="${data.pembelian ? data.pembelian.qty : ''}">
                  <div class="invalid-feedback"></div>
               </div>

							 <div class="col-12 col-md-6 mb-2">
								<label for="price" class="form-label">Harga Per Barang</label>
								<div class="input-group mb-2">
										<span class="input-group-text">Rp</span>
										<input type="text" class="form-control form-pembelian text-capitalize form-number rounded-end" id="harga" name="harga" placeholder="" value="${data.barang ? data.barang.harga : ''}">
										<div class="invalid-feedback"></div>
								</div>
							 </div>

							 <div class="col-12 col-md-6 mb-2">
								<label for="price" class="form-label">Total</label>
								<div class="input-group mb-2">
										<span class="input-group-text">Rp</span>
										<input type="text" class="form-control form-pembelian text-capitalize form-number rounded-end" id="total" name="total" placeholder="" value="${data.barang ? data.barang.total : ''}">
										<div class="invalid-feedback"></div>
								</div>
							 </div>

							 <div class="col mb-2">
                  <label for="nama" class="form-label">Keterangan</label>
                  <textarea class="form-control form-pembelian" id="exampleFormControlTextarea1" rows="3"></textarea>
                  <div class="invalid-feedback"></div>
               </div>

							</div>

            </div>
            <div class="modal-footer border-0">
               <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Cancel</button>
               <button class="btn btn-primary" type="submit" id="btn-submit-pembelian">Submit</button>
            </div>
         </div>
      </div>
   </div>
   `

   return content
}

const modalDeletePembelian = () => {
	return `
	<div class="modal fade" id="delete-modal" tabindex="-1">
	<div class="modal-dialog w-25">
		 <div class="modal-content overflow-hidden">
				<div class="modal-body text-center">
					 <h5>Yakin data akan dihapus?</h6>
					 <p class="fs-6 mb-0">Anda tidak dapat mengembalikan data yang telah dihapus</p>
				</div>
				<form class="row justify-content-center align-items-center p-1 border-top" action="" method="" id="form-delete-pembelian">
					 <div class="col-6 border-end">
							<button type="submit" class="btn btn-sm text-danger btn-transparent w-100 fs-6">Hapus</button>
					 </div>
					 <div class="col-6">
							<button type="button" class="btn btn-sm text-secondary bg-transparent w-100 fs-6" data-bs-dismiss="modal">Batal</button>
					 </div>
				</form>
		 </div>
	</div>
</div>
 `
}

export { modalPembelian, modalDeletePembelian }