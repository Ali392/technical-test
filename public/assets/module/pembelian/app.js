import { alert, reloadTable, requestData, showInvalidMessage } from "../global/global.js"
import { modalDeletePembelian, modalPembelian } from "./menu-pembelian.js"

let tablePembelian, content, isInsert, transaksiId
content = document.querySelector('.content')

tablePembelian = $('#datatable-pembelian').DataTable({
        processing: true,
        serverSide: true,
        // code 1 for pembelian
        ajax: "/transaksi/pembelian",
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
        columns: [
            {data: 'DT_RowIndex', name: 'id', orderable: false, searchable: false},
            {data: 'date', name: 'date'},
            {data: 'name', name: 'name'},
            {data: 'qty', name: 'qty'},
            {data: 'harga', name: 'harga'},
            {data: 'total', name: 'total'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        language: {
            lengthMenu: "_MENU_ Data per halaman",
            //zeroRecords: "Data tidak ditemukan",
            info: "Menampilkan halaman _PAGE_ dari _PAGES_",
            //infoEmpty: "Data kosong",
            infoFiltered: "(filter dari _MAX_ total data)",
            search: "Cari :",
    
            oPaginate: {
                sNext: '<i class="bi bi-arrow-right-short"></i>',
                sPrevious: '<i class="bi bi-arrow-left-short"></i>',
                sFirst: 'Pertama',
                sLast: 'Terakhir'
            },
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-12'i><'col-12'<'d-flex justify-content-center'p>>>",
        drawCallback: () => {

            // // Delete
            const btnDelete = document.querySelectorAll('.delete-data')
            btnDelete.forEach(btn => btn.addEventListener('click', async function () {
                this.disabled = true
                transaksiId = this.dataset.id

                content.insertAdjacentHTML('afterend', modalDeletePembelian())
                const modalDelete = document.getElementById('delete-modal')
                const modal = new bootstrap.Modal(modalDelete)
                modal.show()
                const formDelete = document.querySelector('#form-delete-pembelian')
                formDelete.addEventListener('submit', async (e) => {
                    e.preventDefault()
                    modal.hide()
                    try {
                        const result = await requestData(`/transaksi/pembelian/delete/${transaksiId}`, 'DELETE')
                        alert('success', 'Berhasil', result.message)
                    } catch (error) {
                        alert('error', 'Gagal', error.message) 
                    }

                    reloadTable(tablePembelian)
                })
                modalDelete.addEventListener('hidden.bs.modal', () => {
                    modalDelete.remove()
                    this.disabled = false
                })
            }))
        }
    })

//tambah pembelian
const btnAdd = document.querySelector('#add-pembelian')
btnAdd.addEventListener('click', async function (e) {
    btnAdd.disabled = true
    isInsert = true

    const data = {
        title: 'Tambah transaksi pembelian barang'
    }

    try {
        const response = await requestData('/barang/getAll', 'GET')
        // console.log(response)
        data.barangs = response.barangs
        content.insertAdjacentHTML('afterend', modalPembelian(data))
        const modalAdd = document.getElementById('modal-pembelian')
        const modal = new bootstrap.Modal(modalAdd)
        modal.show()

        // batasi input angka
        const inputAngka = document.querySelectorAll('.form-number')
        inputAngka.forEach(input => input.addEventListener('input', function () {
            this.value = this.value.replace(/[^0-9]/g, "")
        }))

        //get data harga barang
        const selectBarang = document.querySelector('#select_barang')
        selectBarang.addEventListener('change', async function (e) {
            e.preventDefault()

            const result = await requestData(`/barang/${this.value}/edit`, 'GET')
            //get element harga
            // console.log(result.barang)
            const formHarga = document.querySelector('#harga')
            formHarga.value = result.barang.price
        })

        //count total harga
        const formQty = document.querySelector('#qty')
        const formHarga = document.querySelector('#harga')
        const formTotal = document.querySelector('#total')
        formQty.addEventListener('input', function () {
            formTotal.value = this.value * formHarga.value
        })

        submit(isInsert, modal)
        modalAdd.addEventListener('hidden.bs.modal', () => {
           modalAdd.remove()
           btnAdd.disabled = false
        })
    } catch (error) {
        alert('error', 'Gagal', error.message)
    }
  
})

const submit = (isInsert, modal) => {
    const btnSubmit = document.querySelector('#btn-submit-pembelian')
    btnSubmit.addEventListener('click', async (e) => {
        e.preventDefault()

        const formData = new FormData()
        const formPembelian = document.querySelectorAll('.form-pembelian')

        formPembelian.forEach(form => formData.append(form.getAttribute('name'), form.value))

        let url
        
        if (isInsert) {
            url = '/transaksi/pembelian/store'
        } else {
            url = `/transaksi/pembelian/update/${pembelianId}`
            formData.append('_method', 'PATCH')
        }

        try {
            const result = await requestData(url, 'POST', formData)
            alert('success', 'Berhasil', result.message)
            modal.hide()
            reloadTable(tablePembelian)
        } catch (error) {
            // error validation
            if (error.status == 422) {
                showInvalidMessage('form-pembelian', error.validation)
                return false
            }
 
            alert('error', 'Gagal', error.message)
        }
    })

}