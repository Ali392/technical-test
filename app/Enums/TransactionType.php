<?php declare(strict_types=1);

namespace App\Enums;

use App\Services\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class TransactionType extends Enum
{
    const PEMBELIAN = 1;
    const PENJUALAN = 2;

    public static function asBadge($value)
    {
        switch ($value) {
            case (self::PEMBELIAN):
                return 'badge bg-warning';
            case (self::PENJUALAN):
                return 'badge bg-success';
        }
    }
}
