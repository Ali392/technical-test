<?php

namespace App\Http\Controllers;

use App\Enums\TransactionType;
use App\Models\Barang;
use App\Models\TransaksiBarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexPembelian()
    {
        if(request()->ajax()) {
            return $this->getData(TransactionType::PEMBELIAN);
        }

        return view('dashboard.pembelian.index');
    }

    public function indexPenjualan()
    {
        if(request()->ajax()) {
            return $this->getData(TransactionType::PENJUALAN);
        }

        return view('dashboard.penjualan.index');
    }

    //get index by type
    public function getData($type)
    {
        if($type == TransactionType::PEMBELIAN) {
            $data = TransaksiBarang::with('barang')->where('type_id', TransactionType::PEMBELIAN)->orderBy('date', 'desc')->get();
        } else {
            $data = TransaksiBarang::with('barang')->where('type_id', TransactionType::PENJUALAN)->orderBy('date', 'desc')->get();
        }
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($transaksi) {
                return '
                    <div class="d-flex">
                        <button class="btn btn-danger btn-sm delete-data" data-id="' . $transaksi->id . '" type="button"><i class="bi bi-trash"></i></button>
                    </div>
                    ';
            
            })
            ->editColumn('harga', function ($transaksi) {
                return 'Rp. ' . number_format($transaksi->harga, 0, ',', '.');
            })
            ->editColumn('total', function ($transaksi) {
                return 'Rp. ' . number_format($transaksi->total, 0, ',', '.');
            })
            ->editColumn('qty', function ($transaksi) {
                return number_format($transaksi->qty, 0, ',', '.');
            })
            ->editColumn('name', function ($transaksi) {
                return $transaksi->barang->name;
            })
            ->rawColumns(['action'])
            ->make('true');
    }

    public function storePembelian(Request $request)
    {
        if ($request->ajax()) {
            return $this->store($request->all(), TransactionType::PEMBELIAN);
        }
    }

    public function storePenjualan(Request $request)
    {
        if ($request->ajax()) {
            return $this->store($request->all(), TransactionType::PENJUALAN);
        }
    }

    public function store($data, $type)
    {
        $rules = [
            'date' => 'required',
            'barang_id' => 'required',
            'qty' => 'required',
            'harga' => 'required',
            'total' => 'required',
            'description' => 'nullable',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
        ];

        $aliases = [
            'date' => 'Tanggal',
            'barang_id' => 'Nama barang',
            'qty' => 'Jumlah barang',
            'harga' => 'Harga barang',
            'total' => 'Total harga pembelian',
            'descrition' => 'Keterangan',
        ];

        $validator = Validator::make($data, $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {

            DB::transaction(function () use ($validator, $type) {
                $validated = $validator->validated();

                if($type == TransactionType::PEMBELIAN) {
                    $validated['type_id'] = TransactionType::PEMBELIAN;
                } else {
                    $validated['type_id'] = TransactionType::PENJUALAN;
                }

                $barang = Barang::find($validated['barang_id']);

                if($type == TransactionType::PEMBELIAN) {
                    $stock = $barang->stock + $validated['qty'];
                } else {
                    $stock = $barang->stock - $validated['qty'];
                }

                if($stock < 0) {
                    return ['status' => 500, 'message' => 'Data gagal ditambahkan, stok barang tidak cukup!'];
                }

                $transaksi = new TransaksiBarang;
                $transaksi->fill($validated);

                //auto increment stock
                if($transaksi->save()) {
                    $barang->update(['stock' => $stock]);
                }
            });

            return ['status' => 200, 'message' => 'Data berhasil ditambahkan'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal ditambahkan' . $th->getMessage()];
        }
    }

    public function deletePembelian(TransaksiBarang $id)
    {
        if (request()->ajax()) {
            try {
                if($id->delete()) {
                    $barang = Barang::find($id->barang_id);
                    $stock = $barang->stock - $id->qty;
                    $barang->update(['stock' => $stock]);
                }
                return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal dihapus'];
            }
        }
    }

    public function deletePenjualan(TransaksiBarang $id)
    {
        if (request()->ajax()) {
            try {
                if($id->delete()) {
                    $barang = Barang::find($id->barang_id);
                    $stock = $barang->stock + $id->qty;
                    $barang->update(['stock' => $stock]);
                }
                return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal dihapus'];
            }
        }
    }
}
