<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class BarangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = Barang::all();
            return DataTables::of($data)
              ->addIndexColumn()
              ->addColumn('action', function ($barang) {
                    return '
                        <div class="d-flex">
                          <button class="btn btn-info btn-sm show-data" data-id="' . $barang->id . '" type="button"><i class="bi bi-eye"></i></button>
                          <button class="btn btn-warning btn-sm mx-1 edit-data" data-id="' . $barang->id . '" type="button"><i class="bi bi-pencil"></i></button>
                          <button class="btn btn-danger btn-sm delete-data" data-id="' . $barang->id . '" type="button"><i class="bi bi-trash"></i></button>
                        </div>
                        ';
                
              })
              ->editColumn('price', function ($barang) {
                return 'Rp. ' . number_format($barang->price, 0, ',', '.');
              })
              ->editColumn('file', function ($barang) {
                return $barang->getFirstMedia('attachments') ? $barang->getFirstMedia('attachments')->file_name . '' : '(Kosong)';
              })
              ->rawColumns(['action'])
              ->make('true');
        }

        return view('dashboard.barang.index');
    }

    public function store(Request $request)
    {
        // return $request->all();
        if ($request->ajax()) {
            $rules = [
                'name' => 'required',
                'stock' => 'required',
                'price' => 'required',
            ];

            $messages = [
                'required' => ':attribute harus diisi',
            ];

            $aliases = [
                'name' => 'Nama barang',
                'stock' => 'Jumlah persediaan',
                'price' => 'Harga barang',
                'file' => 'Attachment',
            ];

            if ($request->file) {
                $rules['file'] = ['max:500'];
            }

            $validator = Validator::make($request->all(), $rules, $messages, $aliases);

            if ($validator->fails()) {
                return ['status' => 422, 'validation' => $validator->getMessageBag()];
            }

            try {

                DB::transaction(function () use ($validator, $request) {
                    $validated = $validator->validated();
                    $barang = Barang::create($validated);

                    if ($request->file){
                        if($request->file('file') != null) {
                            $barang->addMedia($request->file('file'))->toMediaCollection('attachments');
                        }
                    }
                });

                return ['status' => 200, 'message' => 'Data berhasil ditambahkan'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal ditambahkan' . $th->getMessage()];
            }
        }
    }

    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $barang = Barang::find($id);
                return ['status' => 200, 'barang' => $barang];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }

    public function update(Request $request, $id)
    {
        $barang = Barang::where('id', $id)->first();

        $rules = [
            'name' => 'required',
            'stock' => 'required',
            'price' => 'required',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
        ];

        $aliases = [
            'name' => 'Nama barang',
            'stock' => 'Jumlah persediaan',
            'price' => 'harga barang',
        ];

        if ($request->file) {
            $rules['file'] = ['max:500'];
        }

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {
            $validated = $validator->validated();
            $barang->update($validated);
            if ($request->file){
                if($request->file('file') != null) {
                    $barang->clearMediaCollection('attachments');
                    $barang->addMedia($request->file('file'))->toMediaCollection('attachments');
                }
            }
            return ['status' => 200, 'message' => 'Data berhasil diubah'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal diubah'];
        }
    }

    public function destroy(Barang $id)
    {
        if (request()->ajax()) {
            try {
              $id->delete();
              return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            } catch (\Throwable $th) {
              return ['status' => 500, 'message' => 'Data gagal dihapus'];
            }
        }
    }

    public function getAll()
    {
        if (request()->ajax()) {
            try {
                $barangs = Barang::All();
                return ['status' => 200, 'barangs' => $barangs];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }


}
