<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = User::orderBy('id', 'asc')->get();
            return DataTables::of($data)
              ->addIndexColumn()
              ->addColumn('action', function ($user) {
                    return '
                        <div class="d-flex">
                          <button class="btn btn-warning btn-sm mx-1 edit-data" data-id="' . $user->id . '" type="button"><i class="bi bi-pencil"></i></button>
                          <button class="btn btn-danger btn-sm delete-data" data-id="' . $user->id . '" type="button"><i class="bi bi-trash"></i></button>
                        </div>
                        ';
                
              })
              ->editColumn('role_id', function ($user) {
                $role = $user->role ? $user->role->name : "(kosong)";
                return '<span class="badge bg-info">' . $role . '</span>';
              })
              ->rawColumns(['action', 'role_id'])
              ->make('true');
        }

        return view('dashboard.user.index');
    }

    public function store(Request $request)
    {
        $rules = [
            'role_id' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
            'email' => ':attribute harus valid',
            'same' => ':attribute harus sama dengan password',
            'unique' => ':attribute sudah dipakai',
            'min' => ':attribute harus lebih dari :min karakter',
        ];

        $aliases = [
            'role_id' => 'Role',
            'name' => 'Nama',
            'email' => 'Email',
            'password' => 'Password',
            'password_confirmation' => 'Konfirmasi Password',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {
            $password = Hash::make($request->password);
            $validated = $validator->validated();
            $validated['password'] = $password;
            $user = User::create($validated);
            $user->save();

            return ['status' => 200, 'message' => 'Data berhasil ditambahkan'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal ditambahkan'];
        }
    }

    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $user = User::find($id);
                $roles = UserRole::All();
                return ['status' => 200, 'user' => $user, 'roles' => $roles];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }

    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->first();

        $rules = [
            'role_id' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ];

        $messages = [
            'required' => ':attribute harus diisi',
            'email' => ':attribute harus valid',
            'same' => ':attribute harus sama dengan password',
            'unique' => ':attribute sudah dipakai',
            'min' => ':attribute harus lebih dari :min karakter',
        ];

        $aliases = [
            'role_id' => 'Role',
            'name' => 'Nama',
            'email' => 'Email',
            'password' => 'Password',
            'password_confirmation' => 'Konfirmasi Password',
        ];

        if ($request->password) {
            $rules['password'] = ['min:6'];
            $rules['password_confirmation'] = ['same:password'];
        }

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {
            $validated = $validator->validated();
            if($request->password) {
                $validated['password'] = Hash::make($request->password);
            }
            $user->update($validated);

            return ['status' => 200, 'message' => 'Data berhasil diubah'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal diubah'];
        }
    }

    public function destroy(User $id)
    {
        if (request()->ajax()) {
            try {
                $id->delete();
                return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal dihapus'];
            }
        }
    }

    public function export() 
    {
        return Excel::download(new UserExport, 'data-user.xlsx');
    }
}
