//import vue router
import { createRouter, createWebHistory } from 'vue-router'

import AllProduct from '../components/AllProduct.vue';
import CreateProduct from '../components/CreateProduct.vue';
import EditProduct from '../components/EditProduct.vue';
 
const routes = [
    {
        name: 'products.index',
        path: '/product',
        component: AllProduct
    },
    {
        name: 'products.create',
        path: '/products/create',
        component: CreateProduct
    },
    {
        name: 'products.edit',
        path: '/products/edit/:id',
        component: EditProduct
    }
];

//create router
const router = createRouter({
    history: createWebHistory(),
    routes // <-- routes,
})

export default router

