@extends('layouts.dashboard-app')
@section('css')
<!-- <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" /> -->
@endsection

@section('content')
<main id="main" class="main content">

    <div id="app"></div>

</main>
@endsection

@section('script')
@vite('resources/js/app.js')
<!-- <script src="{{ mix('js/app.js') }}" type="module"></script> -->
@endsection