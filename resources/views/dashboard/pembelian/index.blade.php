@extends('layouts.dashboard-app')
@section('content')
<main id="main" class="main content">

    <div class="pagetitle">
      <h1>Pembelian Barang</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
          <li class="breadcrumb-item active">Pembelian Barang</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
			<div class="row">
					<div class="col d-flex justify-content-end p-2">
							<a class="btn btn-primary" href="#" id="add-pembelian">
									<i class="bi bi-plus-square me-1"></i>
									Tambah pembelian
							</a>
					</div>
			</div>

      <div class="row">
        <div class="col">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Pembelian Barang</h5>

              <!-- Default Table -->
              <table class="table" id="datatable-pembelian">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Nama Barang</th>
                    <th scope="col">Qty</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Total</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>
      </div>
    </section>

  </main>
@endsection

@section('script')
<script src="{{ asset('assets/module/pembelian/app.js') }}" type="module"></script>
@endsection