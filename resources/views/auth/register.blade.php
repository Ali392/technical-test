@extends('layouts.auth-app')
@section('content')
	<section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
		<div class="container">
				<div class="row justify-content-center">
						<div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

								<div class="d-flex justify-content-center py-4">
									<a href="index.html" class="logo d-flex align-items-center w-auto">
										<img src="{{ asset('assets/img/logo.png') }}" alt="logo">
										<span class="d-none d-lg-block">NiceAdmin</span>
									</a>
								</div><!-- End Logo -->

								<div class="card mb-3">

										<div class="card-body">

												<div class="pt-4 pb-2">
														<h5 class="card-title text-center pb-0 fs-4">Register</h5>
														<p class="text-center small">Masukkan data anda</p>
												</div>

                                                @if ($errors->any())
                                                @foreach ($errors->all() as $error)
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="padding:.25rem;">
                                                    {{ $error }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" style="padding:.5rem;"></button>
                                                </div>
                                                @endforeach
                                                @endif

												<form class="row g-3 needs-validation" novalidate method="POST" action="{{ route('register') }}">
														@csrf

                                                        <div class="col-12">
																<label for="yourUsername" class="form-label">Name :</label>
																<div class="input-group has-validation">
																		<!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
																		<input type="text" name="name" value="{{ old('name') }}"
																				class="form-control @error('name') is-invalid @enderror" id="name" required>

																		@error('name')
																				<span class="invalid-feedback" role="alert">
																						<strong>{{ $message }}</strong>
																				</span>
																		@enderror
																</div>
														</div>

														<div class="col-12">
																<label for="yourUsername" class="form-label">Email :</label>
																<div class="input-group has-validation">
																		<!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
																		<input type="email" name="email" value="{{ old('email') }}"
																				class="form-control" id="email" required>

																		@error('email')
																				<span class="invalid-feedback" role="alert">
																						<strong>{{ $message }}</strong>
																				</span>
																		@enderror
																</div>
														</div>

														<div class="col-12">
																<label for="yourPassword" class="form-label">Password :</label>
																<div class="input-group has-validation">
																		<input type="password" name="password"
																				class="form-control @error('password') is-invalid @enderror" id="password"
																				required autocomplete="new-password">
																		<div class="position-absolute" style="top: 50%; left: 95%; transform: translate(-50%, -50%); cursor: pointer;z-index:1000;">
																				<span class="toggle-eye text-dark">
																				<i class="bi bi-eye-fill"></i>
																				</span>
																		</div>

																		@error('password')
																				<span class="invalid-feedback" role="alert">
																						<strong>{{ $message }}</strong>
																				</span>
																		@enderror
																</div>
														</div>

                                                        <div class="col-12">
																<label for="yourPassword" class="form-label">Konfirmasi Password :</label>
																<div class="input-group has-validation">
																		<input type="password" name="password_confirmation"
																				class="form-control" id="password-confirm"
																				required autocomplete="new-password">
																		<div class="position-absolute" style="top: 50%; left: 95%; transform: translate(-50%, -50%); cursor: pointer;z-index:1000;">
																				<span class="toggle-eye-confirm text-dark">
																				<i class="bi bi-eye-fill"></i>
																				</span>
																		</div>
																</div>
														</div>

														<hr class="mt-4 mb-2">

														<div class="col-12 mb-2">
																<button class="btn btn-primary w-100" type="submit">Register</button>
														</div>
														<div class="col-12">
															<p class="small mb-0">Have an account? <a href="{{ route('login') }}">Login here</a></p>
														</div>
												</form>

										</div>
								</div>

						</div>
				</div>
		</div>

</section>
@endsection
