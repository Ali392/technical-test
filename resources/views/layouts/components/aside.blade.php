<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*home*') ? '' : 'collapsed' }}" href="{{ route('home') }}">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-heading">Setting</li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*role*') ? '' : 'collapsed' }}" href="{{ route('role.index') }}">
          <i class="bi bi-people"></i>
          <span>Role User</span>
        </a>
      </li>

      @if(auth()->user()->role_id == 1)
      <li class="nav-item">
        <a class="nav-link {{ Request::is('*user*') ? '' : 'collapsed' }}" href="{{ route('user.index') }}">
          <i class="bi bi-people"></i>
          <span>Akun User</span>
        </a>
      </li>
      @endif

      <li class="nav-heading">Menu</li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*barang*') ? '' : 'collapsed' }}" href="{{ route('barang.index') }}">
          <i class="bi bi-card-list"></i>
          <span>Persedian Barang</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*transaksi/pembelian*') ? '' : 'collapsed' }}" href="{{ route('transaction.pembelian.index') }}">
          <i class="bi bi-card-list"></i>
          <span>Pembelian Barang</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*transaksi/penjualan*') ? '' : 'collapsed' }}" href="{{ route('transaction.penjualan.index') }}">
          <i class="bi bi-card-list"></i>
          <span>Penjualan Barang</span>
        </a>
      </li>

      {{-- <li class="nav-item">
        <a class="nav-link {{ Request::is('*product*') ? '' : 'collapsed' }}" href="{{ route('product.index') }}">
          <i class="bi bi-card-list"></i>
          <span>Persedian Produk</span>
        </a>
      </li> --}}

    </ul>

  </aside>