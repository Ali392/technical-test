<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return redirect()->route('login');
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Role Routes
Route::name('role.')->group(function () {
    Route::get('/role', [RoleController::class, 'index'])->name('index');
    Route::post('/role/store', [RoleController::class, 'store'])->name('store');
    Route::get('/role/{id}/edit', [RoleController::class, 'edit'])->name('edit');
    Route::patch('/role/update/{id}', [RoleController::class, 'update'])->name('update');
    Route::delete('/role/delete/{id}', [RoleController::class, 'destroy'])->name('delete');
    Route::get('/role/getAll', [RoleController::class, 'getAll']);
});

//User Routes
Route::name('user.')->group(function () {
    Route::get('/user', [UserController::class, 'index'])->name('index');
    Route::post('/user/store', [UserController::class, 'store'])->name('store');
    Route::get('/user/{id}/edit', [UserController::class, 'edit'])->name('edit');
    Route::patch('/user/update/{id}', [UserController::class, 'update'])->name('update');
    Route::delete('/user/delete/{id}', [UserController::class, 'destroy'])->name('delete');
    Route::get('/user/export', [UserController::class, 'export'])->name('export');
});

// Barang Routes
Route::name('barang.')->group(function () {
    Route::get('/barang', [BarangController::class, 'index'])->name('index');
    Route::post('/barang/store', [BarangController::class, 'store'])->name('store');
    Route::get('/barang/{id}/edit', [BarangController::class, 'edit'])->name('edit');
    Route::patch('/barang/update/{id}', [BarangController::class, 'update'])->name('update');
    Route::delete('/barang/delete/{id}', [BarangController::class, 'destroy'])->name('delete');
    Route::get('/barang/getAll', [BarangController::class, 'getAll']);
});

// Pembelian Routes
Route::name('transaction.pembelian.')->group(function () {
    Route::get('/transaksi/pembelian', [TransactionController::class, 'indexPembelian'])->name('index');
    Route::post('/transaksi/pembelian/store', [TransactionController::class, 'storePembelian'])->name('store');
    Route::delete('/transaksi/pembelian/delete/{id}', [TransactionController::class, 'deletePembelian']);
});

// Penjualan Routes
Route::name('transaction.penjualan.')->group(function () {
    Route::get('/transaksi/penjualan', [TransactionController::class, 'indexPenjualan'])->name('index');
    Route::post('/transaksi/penjualan/store', [TransactionController::class, 'storePenjualan'])->name('store');
    Route::delete('/transaksi/penjualan/delete/{id}', [TransactionController::class, 'deletePenjualan']);
});

Route::name('product.')->group(function () {
    Route::get('/product', [ProductController::class, 'index'])->name('index');
});
